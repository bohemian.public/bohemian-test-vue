# bohemian-test-vue

Bohemian Studio testing Nuxt/Vue project.

This is a simple Vue+Nuxt.js application generated using the default public Nuxt.js starter template.

Nuxt is a convention-over-configuration meta-framework that simplifies and speeds up Vue universal application development.

For each task, please create a separate git branch and after you're finished coding, publish it on your GitLab profile. Point us to this repository and branch.

## Prerequisities

Installed Node 8.0+

## Documentation

Vue:  https://vuejs.org/v2/guide/

Vue Single-File-Components:  https://vuejs.org/v2/guide/single-file-components.html

Nuxt: https://nuxtjs.org/guide

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Task 1

After runing the dev task with NPM, you can navigate to http://localhost:3000/artists.

You will see a simple webpage with a table containing some information about music artists.

The source code to this page can be found at /pages/artists.vue

Your task is:

1. Move the table from the page component to a stand-alone component that will be located in /components.
2. This component should accept a property that will contain the list of artists to be rendered in the table.
3. The table will allow the user to sort the passed data using any column. (BONUS: The sorting will ignore "The" in the artists' names)
4. BONUS 2: The table will allow the user to filter the data by name of the artist using a separate text input field.
5. Don't forget to implement your stand-alone component into the artists.vue page, replacing the existing table.
6. BONUS 3: If you correct one factual mistake in the artists' data, you get a free beverage of your choice from Andrej.

